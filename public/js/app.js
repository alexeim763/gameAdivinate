"use strict";

var $ = window.jQuery;

//Print Cards

//API DeckOfCards http://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1
var getDeckIDURL = "http://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1";
var deckURL = "http://deckofcardsapi.com/api/deck/";
var deck_id = GetDeckId(getDeckIDURL); //Promise
var deckPromise = [];
var deck = [];
var counterCicles = 1;
var secondMessage = "Estamos pensando, ahora vuelve a elegir la fila en la que se encuentra tu carta";

$('.Layout-buttonSelectCard').on('click', function (event) {
	//cuando hago click pongo toda la magia del algoritmo
	event.preventDefault();
	$('.Message-text').html('');
	$('.Message-text').hide().append(secondMessage).fadeIn();
	var $this = $(this);
	var numberDeckSelected = $this.data("number") - 1;
	var deckSelected = deckPromise[numberDeckSelected];
	console.log(deckSelected);
	// copy $deckSelected to var temp "tempDeckPromise "
	deckPromise = suffleDeckSelected(deckSelected, numberDeckSelected);
	printAllCard(deckPromise);

	if (counterCicles == 1) {
		deck = deckSelected;
	}
	var tempDeck = deckSelected.filter(function (elem) {
		if (deck.indexOf(elem) != -1) return true;

		return false;
	});

	deck = tempDeck;
	counterCicles += 1;
	if (deck.length == 1) {
		$('.Message').fadeOut('slow');
		var $layout = $('.Layout-board');
		$layout.html('');
		var $template = "\n\t\t\t<div class=\"Answer\">\n\t\t\t\t<h1 class=\"Answer-text\">Ya lo sé, esta es tu carta verdad?</h1>\n\t\t\t\t<div class=\"Answer-card\">\n\t\t\t\t\t<div class=\"Card\">\n\t\t\t\t\t\t<img src=\"" + deck[0].image + "\" alt=\"1 Espadas\" class=\"Card-imageCardAnswer\"/>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"fb-like\" data-href=\"https://developers.facebook.com/docs/plugins/\" data-layout=\"button_count\" data-action=\"like\" data-show-faces=\"true\" data-share=\"true\"></div>\n\t\t\t<div>\n\t\t";
		$layout.hide().append($($template)).fadeIn(2000);
	}
});

deck_id.then(function (id) {
	var url = "" + deckURL + id + "/draw/?count=8";
	//console.log(url)

	var _loop = function (x) {
		getDeck(url).then(function (deck) {
			deckPromise.push(deck);
			//deck Completo
			console.log(deckPromise);
			for (var i in deck) {
				renderCard(deck[i], x);
			}
		});
	};

	for (var x = 0; x < 3; x++) {
		_loop(x);
	}
});

function GetDeckId(url) {
	return Promise.resolve($.get(url)).then(function (deck) {
		var data = deck.deck_id;
		return Promise.resolve(data);
	})["catch"](function (err) {
		console.log(err);
	});
}

function getDeck(url) {
	return Promise.resolve($.get(url)).then(function (cards) {
		return Promise.resolve(cards.cards);
	});
}

function renderCard(deck, id) {
	var $template = "\n\t\t\t<div class=\"Card\">\n\t\t\t\t<img src=\"" + deck.image + "\" alt=\"1 Espadas\" class=\"Card-imageCard\"/>\n\t\t\t</div>$la\n\t\t";
	var layout = $('.Layout-cards')[id];
	var $layout = $(layout);
	$layout.hide().append($($template)).fadeIn('slow');
}

function suffleDeckSelected(deckSelected, numberDeckSelected) {
	var tempDeckPromise = [[], [], []];
	var counterRow = 0; // this var is a counter for 3 rows
	var counterItems = 0;
	for (var i = 0; i < tempDeckPromise.length; i++) {
		for (var x in deckSelected) {
			tempDeckPromise[counterRow][counterItems] = deckSelected[x];
			counterRow++;

			if (counterRow == 3) {
				counterRow = 0;
				counterItems++;
			}
		}
		numberDeckSelected++;
		if (numberDeckSelected == 3) {
			numberDeckSelected = 0;
		}
		deckSelected = deckPromise[numberDeckSelected];
	}
	return tempDeckPromise;
}

function printAllCard(completeDeck) {
	$('.Card').html('');
	for (var i = 0; i < completeDeck.length; i++) {
		var _deck = completeDeck[i];
		for (var x in _deck) {
			renderCard(_deck[x], i);
		}
	}
}
