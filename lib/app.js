var $ = window.jQuery

//Print Cards

//API DeckOfCards http://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1
let getDeckIDURL = "http://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1"
let deckURL = "http://deckofcardsapi.com/api/deck/"
let deck_id = GetDeckId(getDeckIDURL) //Promise
let deckPromise = []
let deck = []
let counterCicles = 1
let secondMessage = "Estamos pensando, ahora vuelve a elegir la fila en la que se encuentra tu carta"

$('.Layout-buttonSelectCard').on('click', function (event) {
	event.preventDefault()
	$('.Message-text').html('')
	$('.Message-text').hide().append(secondMessage).fadeIn();
	let $this = $(this)
	let numberDeckSelected = $this.data("number")-1
	let deckSelected = deckPromise[numberDeckSelected]
	deckPromise = suffleDeckSelected(deckSelected,numberDeckSelected)	
	printAllCard(deckPromise)


	if (counterCicles == 1){
		deck = deckSelected
	}
	let tempDeck = deckSelected.filter(function(elem) {
		if(deck.indexOf(elem) != -1) return true

		return false
	});

	
	deck = tempDeck
	counterCicles += 1
	if(deck.length == 1)
	{
		$('.Message').fadeOut('slow')
		let $layout = $('.Layout-board')
		$layout.html('')
		let $template = `
			<div class="Answer">
				<h1 class="Answer-text">Ya lo sé, esta es tu carta verdad?</h1>
				<div class="Answer-card">
					<div class="Card">
						<img src="${deck[0].image}" alt="1 Espadas" class="Card-imageCardAnswer"/>
					</div>
				</div>
				<div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
			<div>
		`
		$layout.hide().append($($template)).fadeIn(2000);
	}
})

deck_id							
.then((id) => {
	let url = `${deckURL}${id}/draw/?count=3`
	//console.log(url)
	for( let x = 0; x < 3 ; x++){
		getDeck(url)
		.then((deck) =>{
			deckPromise.push(deck)
			//deck Completo
			console.log(deckPromise)
			for(let i in deck) {
				renderCard(deck[i],x)
			}
		})	
	}
})


function GetDeckId(url) {
	return Promise.resolve($.get(url))
	.then((deck) => {
		let data = deck.deck_id
		return Promise.resolve(data)
	})
	.catch((err) => {
		console.log(err)
	})
}

function getDeck(url){
	return Promise.resolve($.get(url))
				.then((cards) => {
					return Promise.resolve(cards.cards)
				})
}


function renderCard(deck,id){
		let $template = `
			<div class="Card">
				<img src="${deck.image}" alt="1 Espadas" class="Card-imageCard"/>
			</div>$la
		`
		let layout = $('.Layout-cards')[id]
		let $layout = $(layout)
		$layout.hide().append($($template)).fadeIn('slow')
	
}

function suffleDeckSelected(deckSelected,numberDeckSelected){
	let tempDeckPromise = [[],[],[]]
	let counterRow = 0 // this var is a counter for 3 rows
	let counterItems = 0
	for(let i = 0 ; i < tempDeckPromise.length ; i++){
		for(let x in deckSelected){
			tempDeckPromise[counterRow][counterItems] = deckSelected[x]
			counterRow++

			if(counterRow == 3){
				counterRow = 0
				counterItems++
			}
		}
		numberDeckSelected++
		if(numberDeckSelected == 3)
		{
			numberDeckSelected = 0
		}
		deckSelected = deckPromise[numberDeckSelected]
	}
	return tempDeckPromise
}

function printAllCard(completeDeck){
	$('.Card').html('')
	for(let i = 0 ; i < completeDeck.length ; i++){
		let deck = completeDeck[i]
		for (let x in deck){
			renderCard(deck[x],i)			
		}
	}

}
